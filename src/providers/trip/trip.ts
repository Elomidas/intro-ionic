import { Injectable } from '@angular/core';

import { Trip } from '../../models/trip'

/*
  Generated class for the TripProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TripProvider {

  tripList: Trip[] = [];

  constructor() {
    this.tripList = [];
    this.tripList.push(new Trip("Allemagne","C'est long","assets/imgs/al.jpg"));
    this.tripList.push(new Trip("Pologne","C'est chiant","assets/imgs/pl.jpg"));
    this.tripList.push(new Trip("France","C'est Orléans","assets/imgs/fr.jpg"));
  }

  getTrips() : Trip[] {
    return this.tripList;
  }
}
