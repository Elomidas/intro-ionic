import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TripsPage } from '../trips/trips';
import { SearchPage } from '../search/search';
import { SettingsPage } from '../settings/settings';

/**
 * Generated class for the TabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tripsRoot = TripsPage
  searchRoot = SearchPage
  settingsRoot = SettingsPage


  constructor(public navCtrl: NavController) {}

}
