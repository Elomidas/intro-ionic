import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { TripProvider } from '../../providers/trip/trip';
import { Trip } from '../../models/trip';
import { DetailsPage } from '../details/details';
import { AddTripPage } from '../add-trip/add-trip';

/**
 * Generated class for the TripsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-trips',
  templateUrl: 'trips.html',
})
export class TripsPage {

  myTrips: Trip[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public tripProvider: TripProvider,
    public modCtrl: ModalController) {
    this.myTrips = this.tripProvider.getTrips();
    console.log(this.myTrips.length);
  }

  ionViewDidLoad() {
  }

  goToDetails(trip: Trip) {
    this.navCtrl.push(DetailsPage, {trip: trip});
  }

  addTrip() {
    let modal = this.modCtrl.create(AddTripPage);
    modal.present();
  }

}
