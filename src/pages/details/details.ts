import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Trip } from '../../models/trip';

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  trip: Trip;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.trip = this.navParams.get('trip');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

}
