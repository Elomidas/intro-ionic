import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { TripsPage } from '../pages/trips/trips';
import { SearchPage } from '../pages/search/search';
import { SettingsPage } from '../pages/settings/settings';
import { DetailsPage } from '../pages/details/details';
import { TripProvider } from '../providers/trip/trip';
import { AddTripPage } from '../pages/add-trip/add-trip';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    TripsPage,
    SettingsPage,
    SearchPage,
    DetailsPage,
    AddTripPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    TripsPage,
    SettingsPage,
    SearchPage,
    DetailsPage,
    AddTripPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TripProvider
  ]
})
export class AppModule {}
